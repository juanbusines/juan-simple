package net.ligamx.test.run;

public class ForLoop {
	
	public static void main(String[] args) {
		
		
		int variable = 4;
		
		for(int i = 1; i <= 20; i++) {
			
			System.out.println( variable + " X " + i + " = " + (i * variable));
			
		}
	

	}
}
