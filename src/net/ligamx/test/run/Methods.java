package net.ligamx.test.run;

import java.util.Scanner;

public class Methods {

	static Scanner scanner = new Scanner(System.in);

	public static int addtion(int numberOne, int numberTwo) {
		return numberOne + numberTwo;
	}

	public static int substraction(int numberOne, int numberTwo) {
		return numberOne - numberTwo;
	}

	public static double divide(int numberOne, int numberTwo) {
		return numberOne / numberTwo;
	}
	
	
	public static String chain() {
		return "Give me first number :";
	}
	
	
	public static int giveMeFirst() {	
		System.out.println(chain());
		return scanner.nextInt();
	}
	
	
	public static int giveMeSecond() {
		System.out.println("Give me Second number :");
		return scanner.nextInt();
	}

	public static void main(String[] args) {

		int numberOne = giveMeFirst();
		int numberTwo = giveMeSecond();

		System.out.println("addtion = " + addtion(numberOne, numberTwo));

		System.out.println("substraction = " + substraction(numberOne, numberTwo));

		System.out.println("divide = " + divide(numberOne, numberTwo));

	}

}
