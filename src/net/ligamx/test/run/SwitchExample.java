package net.ligamx.test.run;

public class SwitchExample {

	public static void main(String[] args) {

		UserUtils userUtils = new UserUtils();

		String optionContinue = "n";

		do {

			char option = userUtils.giveMeYourOptionChar(" a) Saludo  \n b) Despedida  \n c) Groseria");
			
			

			switch (option) {

			case 'a':
				System.out.println("Hi");
				break;

			case 'b':
				System.out.println("GoodBye");
				break;

			case 'c':
				System.out.println("Buena salvada");
				break;

			default:
				System.out.println("Invalid  option");
				break;
			}

			optionContinue = userUtils.doYouWishContinue();
			

		} while (optionContinue.equals("y"));

		System.out.println("Finished");

	}

}
