package net.ligamx.test.run;

import java.util.Scanner;

public class UserUtils {

	Scanner scanner = new Scanner(System.in);

	public int giveMeFirst() {
		System.out.println("Give me first number");
		return scanner.nextInt();
	}

	public int giveMeSecond() {
		System.out.println("Give me first second");
		return scanner.nextInt();
	}
	
	
	public int giveMeYourOption(String options) {
		System.out.println("Give me your option ");
		System.out.println(options);
		return scanner.nextInt();
	}
	
	
	public char giveMeYourOptionChar(String options) {
		System.out.println("Give me your option ");
		System.out.println(options);
		return  scanner.next().charAt(0);
	}
	
	
	public String doYouWishContinue() {
		System.out.println("Do you wish continue ?  <<y>> <<n>>");
		return scanner.next();
	}

}
