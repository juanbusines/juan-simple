package net.ligamx.test.run.pancho.homework;

import java.util.Scanner;

public class TablasMultiplicar {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		String respuesta;
		
		do {
			
			int numero;

			System.out.println("Ingresa la tabla que quieres:");
			numero = scanner.nextInt();

			for( int i = 1 ; i <= 10 ; i++) {
				
				System.out.println(numero + "x" + i + "=" + (i * numero));
				
			}

			System.out.println("Desea hacer otra? <<y>> <<n>>");
			respuesta = scanner.next();

		} while (respuesta.equals("y"));
		System.out.println("Adios, ya aprendete las putas tablas");

	}
}